import React from 'react'

const Header = (props) => (
    <header id="header" style={props.timeout ? {display: 'none'} : {}}>
        <div className="content">
            <div className="inner">
                <h1>Website security audit & consulting</h1>
                 <p> More than 70% websites are hackable and have breaches. We can help you to find security weaknesses and vulnerabilities.</p>
            </div>

            <p className="url">Enter an URL and the scanner will check the website for known vunerabilities, malware, website errors, and out-of-date software.</p>

            <form name="contact" method="post" action="#" className="url"  data-netlify="true" data-netlify-honeypot="bot-field">
                <ul className="actions-wstr"><li className="urlstr"><input placeholder="Scan Website" type="text" name="scan" id="websiteurl" className="url"/></li><li className="urlbtn"><input type="submit" value="Scan Website" className="special urlbtn"/></li></ul>
            </form>

            <p className="disclaimer"><strong>Disclaimer:</strong> this is a free website security scanner. </p><p className="disclaimer next">Remote scanners have limited access and results are not guaranteed. </p><p className="disclaimer last">For a full scan, contact our team.</p>

        </div >

        <a href="javascript:;" onClick={() => {props.onOpenArticle('report')}}>Sample report</a>


        <nav >
            <ul>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('intro')}}>WAPT Seсurity</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('products')}}>Products</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('pricing')}}>Pricing</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('about')}}>About Us</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('why')}}>Why WAPT?</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('contact')}}>Contact Us</a></li>
            </ul>
        </nav>
    </header>
)

Header.propTypes = {
    onOpenArticle: React.PropTypes.func,
    timeout: React.PropTypes.bool
}

export default Header

