import React from 'react'
import Link from 'gatsby-link'

import pic01 from '../images/pic01.jpg'
import pic02 from '../images/pic02.jpg'
import pic03 from '../images/pic03.jpg'

import debian from '../assets/logo-images/Debian.png'
import nginx from '../assets/logo-images/Nginx.svg'
import php from '../assets/logo-images/PHP.svg'
import swf from '../assets/logo-images/SWF.png'
import liveInternet from '../assets/logo-images/Liveinternet.png'
import jQuery from '../assets/logo-images/jQuery.svg'

class Main extends React.Component {
  render() {

    let close = <div className="close" onClick={() => {this.props.onCloseArticle()}}></div>

    return (
      <div id="main" style={this.props.timeout ? {display: 'flex'} : {display: 'none'}}>

        <report id="report" className={`${this.props.article === 'report' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <div className='tm'><p className='tm'>WAPT Security</p></div>

          <h1>Website Vulnerability Scanner Report</h1>
          <p className='report-disclaimer'>This is a Light mode of free website security scanner. <br />For a full scan, contact our team.</p>

          <p className='info'>Website url: <strong>http://mediam.ru</strong></p>
          <p className='info'>Scan type:	Light</p>


          <h2>Server software and technology found</h2>
          <div className="divTable"><div className="divTableBody">
            <div className="divTableRow">
                <div className="divTableHead">Software / Version</div>
                <div className="divTableHead">Category</div>
                </div>          
            <div className="divTableRow">
              <div className="divTableCell"><img className='icon' src={debian} alt="" />Debian</div>
              <div className="divTableCell">Operating Systems</div></div>
            <div className="divTableRow">
              <div className="divTableCell"><img className='icon' src={nginx} alt="" />Nginx	Web Servers</div>
              <div className="divTableCell">&nbsp;</div></div>
            <div className="divTableRow">
              <div className="divTableCell"><img className='icon' src={php} alt="" />PHP 5.2.17</div>
              <div className="divTableCell">Programming Languages</div></div>
            <div className="divTableRow">
              <div className="divTableCell"><img className='icon' src={swf} alt="" />SWFObject</div>
              <div className="divTableCell">Miscellaneous</div></div>
            <div className="divTableRow">
              <div className="divTableCell"><img className='icon' src={liveInternet} alt="" />Liveinternet</div>
              <div className="divTableCell">Analytics</div></div>
            <div className="divTableRow">
              <div className="divTableCell"><img className='icon' src={jQuery} alt="" />jQuery</div>
              <div className="divTableCell">JavaScript Frameworks</div></div>
          </div></div>


          <h2>Risk ratings:</h2>

          <div className="divTable chart"><div className="divTableBody">
            <div className="divTableRow-chart">
              <div className="divTableCell barname">High: </div>
              <div className="divTableCell barbody high w20">2</div></div>
            <div className="divTableRow-chart">
              <div className="divTableCell barname">Middle:</div>
              <div className="divTableCell barbody mid w20">2</div></div>
            <div className="divTableRow-chart">
              <div className="divTableCell barname">Low:</div>
              <div className="divTableCell barbody low w40">4</div></div>
            <div className="divTableRow-chart">
              <div className="divTableCell barname">Info:</div>
              <div className="divTableCell barbody info w20">2</div></div>
          </div></div>


          <h2>Missing HTTP security headers</h2>

            <div className="divTable"><div className="divTableBody">
              <div className="divTableRow">
                <div className="divTableHead">HTTP Security Header</div>
                <div className="divTableHead">Header Role</div>
                <div className="divTableHead">Status</div>
                </div>
              <div className="divTableRow">
                <div className="divTableCell">X-Frame-Options</div>
                <div className="divTableCell">Protects against Clickjacking attacks</div>
                <div className="divTableCell">Not set</div>
                </div>
              <div className="divTableRow">
                <div className="divTableCell">X-XSS-Protection</div>
                <div className="divTableCell">Mitigates Cross-Site Scripting (XSS) attacks</div>
                <div className="divTableCell">Not set</div>
              </div>
              <div className="divTableRow">
                <div className="divTableCell">X-Content-Type-Options</div>
                <div className="divTableCell">Prevents possible phishing or XSS attacks</div>
                <div className="divTableCell">Not set</div>
              </div>
            </div></div>

          <h2>List of tests performed (10/10)</h2>
          <ul>
              <li>Fingerprinting the server software and technology...</li>
              <li>Checking for vulnerabilities of server-side software...</li>
              <li>Analyzing the security of HTTP cookies...</li>
              <li>Analyzing HTTP security headers...</li>
              <li>...</li>
          </ul>
          {close}
        </report>

        <article id="intro" className={`${this.props.article === 'intro' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h1 className="major">About WAPT Sequrity</h1>
          <span className="image main"><img src={pic01} alt="" /></span>
          <p>WAPT Security is a simple to use security tools built to audit website and test for potential breaches. Build in the cloud. Helps to check alerts and vulnerabilities. Necessary if you want to check your site for GDPR compliance.
          <br /><br />No installation, online tools, easy to use. Simple and effective website security tool(s).
          <br /><br />We help businesses mitigate the risks of unexpected penetrations, data leakage and associated legal risks, service breakdown and website collapse. GDPR, HIPAA and similar government and business standards are taken into account as well.</p>
          {close}
        </article>

        <article id="products" className={`${this.props.article === 'products' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h1 className="major">Product(s)</h1>
          <span className="image main"><img src={pic02} alt="" /></span>
          <p> WAPT - Online testing tools that search for and check 400+ known potential vulnerabilities. </p>
          <p> Name website, confirm email and website ownership, start testing and get report to email.<br /></p>
 
          <p>WSOP - Performance testing for web development teams and QA departments. <a href="https://www.loadtestingtool.com" target="moretool">Learn more</a> .<br /></p>
          {close}
        </article>

        <article id="about" className={`${this.props.article === 'about' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h1 className="major">About Us</h1>
          <span className="image main"><img src={pic02} alt="" /></span>
          <p> Our team have 10+ years experience in website security and building. Client list includes software development teams and website developers, universities and research departments, government agencies, non-profit organizations. We are highly client oriented. <br /></p>
          <p> Our team provide a more specific service upon request. We can assist with the analysis of the results after the test is completed.<br /></p>      

          <a href="javascript:;" onClick={() => alert( "cookies link click" )}>Cookies Policy</a>

          <br /><br />
          <a href="javascript:;" onClick={() => console.log ( "log msg: " )}>txt 2 console</a>

          {close}
        </article>

        <article id="pricing" className={`${this.props.article === 'pricing' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h1 className="major">Pricing</h1>
          <p>Over 80% hackable websites have the same breaches.</p>
          <h2>Free</h2>
          <p> Free to check 1 domain and get report. 1 scan, most basic tool, 100+ vulnerabilities.<br />  </p>

          <h2>Basic</h2>
          <p>15 $/mo, 150 $/year.</p>
          <p>3 websites. Confirmation of ownership of the website is required.</p>
          <p>Basic proposals and alerts for breaches, potential penetrations points and possible data leakages.</p>
          <p>Periodical checking plan according to the schedule you manage. <br /> </p>

          <h2>Pro</h2>
          <p> 25 $/mo, 250 $/year.</p>
          <p>Advanced tools. Assessment of website for GDPR compliance. Periodical checking plan according to the schedule you manage.<br /></p>

          <h2>Business/Enterprise</h2>
          <p> Open for proposals. Websites - unlimited. All the tools.<br /> </p> 
          {close} 
          </article>

        <article id="why" className={`${this.props.article === 'why' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h1 className="major">Why to choose us?</h1>
          <span className="image main"><img src={pic03} alt="" /></span>
          <h3>- Rich functionality </h3>
          <h3>- Low cost </h3>
          <h3>- Easy to use </h3>
          <h3>- High efficiency and scalability </h3>
          <h3>- Technical support and additional services</h3>
          {close}
        </article>

        <article id="contact" className={`${this.props.article === 'contact' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          <h1 className="major">Contact Us</h1>
          <form name="contact" method="post" action="#" data-netlify="true" data-netlify-honeypot="bot-field">
            <div className="field half first">
              <label htmlFor="name">First and Last Name</label>
              <input type="text" name="name" id="name" />
            </div>
            <div className="field half">
              <label htmlFor="email">Email</label>
              <input type="text" name="email" id="email" />
            </div>
            <div className="field half">
              <label htmlFor="email">Company</label>
              <input type="text" name="company" id="company" />
            </div>
            <div className="field half">
              <label htmlFor="email">Position</label>
              <input type="text" name="position" id="position" />
            </div>
            <div className="field">
              <label htmlFor="message">Message</label>
              <textarea name="message" id="message" rows="4"></textarea>
            </div>
            <ul className="actions">
              <li><input type="submit" value="Send Message" className="special" /></li>
              <li><input type="reset" value="Reset" /></li>
            </ul>
          </form>
         {close}
        </article>

        <article id="cookies" className={`${this.props.article === 'cookies' ? 'active' : ''} ${this.props.articleTimeout ? 'timeout' : ''}`} style={{display:'none'}}>
          
          We use cookies on our sites to provide you with the best experience when you browse our website and to improve our site and its contents. By continuing to browse the site, you are agreeing to our use of cookies. 

          {close} 
          </article>  
      </div>
      
    )
  }
}
 

Main.propTypes = {
  route: React.PropTypes.object,
  article: React.PropTypes.string,
  articleTimeout: React.PropTypes.bool,
  onCloseArticle: React.PropTypes.func,
  timeout: React.PropTypes.bool
}

export default Main
