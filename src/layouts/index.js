import React from 'react'
import '../assets/scss/main.scss'
import Helmet from 'react-helmet'

import { OutboundLink } from 'gatsby-plugin-gtag'

import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

class Template extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isArticleVisible: false,
      timeout: false,
      articleTimeout: false,
      article: '',
      loading: 'is-loading'
    }
    this.handleOpenArticle = this.handleOpenArticle.bind(this)
    this.handleCloseArticle = this.handleCloseArticle.bind(this)
  }

  componentDidMount () {
    this.timeoutId = setTimeout(() => {
        this.setState({loading: ''});
    }, 100);
  }

  componentWillUnmount () {
    if (this.timeoutId) {
        clearTimeout(this.timeoutId);
    }
  }

  handleOpenArticle(article) {
    console.log('Article/Open: ' + article);
    console.log(process.env.NODE_ENV + '; dev: ' + process.env.development + '; prod: ' + process.env.production); 
    if ( article === 'report' ) { console.log( 'REPORT !!!')}
    //TODO: send this to GA
    //ga('send', 'event', 'Articles', 'open', article);
    // gtag('event', 'click', {'screen_name': article})
  
    this.setState({
      isArticleVisible: !this.state.isArticleVisible,
      article
    })

    setTimeout(() => {
      this.setState({
        timeout: !this.state.timeout
      })
    }, 325)

    setTimeout(() => {
      this.setState({
        articleTimeout: !this.state.articleTimeout
      })
    }, 350)

  }

  handleCloseArticle() {

    this.setState({
      articleTimeout: !this.state.articleTimeout
    })

    setTimeout(() => {
      this.setState({
        timeout: !this.state.timeout
      })
    }, 325)

    setTimeout(() => {
      this.setState({
        isArticleVisible: !this.state.isArticleVisible,
        article: ''
      })
    }, 350)

  }

  render() {
    const siteTitle = this.props.data.site.siteMetadata.title
    const siteDescription = this.props.data.site.siteMetadata.description
    const { location, children } = this.props

    let rootPath = `/`

    let content;

    if (location.pathname === rootPath) {
      content = (
        <div id="wrapper">
          <Header onOpenArticle={this.handleOpenArticle} timeout={this.state.timeout} />
          <Main
            isArticleVisible={this.state.isArticleVisible}
            timeout={this.state.timeout}
            articleTimeout={this.state.articleTimeout}
            article={this.state.article}
            onCloseArticle={this.handleCloseArticle}
          />
          <Footer timeout={this.state.timeout} />
        </div>
      )
    } else {
      content = (
        <div id="wrapper" className="page">
          <div style={{
            maxWidth: '1140px'
          }}>
            {children()}
          </div>
        </div>
      )
    }

//---  <script dangerouslySetInnerHTML={googleAdsScript}></script> 
   

// <meta name="description" content={siteDescription} />
    return (

      <div className={`body ${this.state.loading} ${this.state.isArticleVisible ? 'is-article-visible' : ''}`}>           
        <Helmet>
            <title>{siteTitle}</title>
            <meta name="description" content={'Simple, fast, effective tools to find out website breaches'} />
            <meta name="keywords" content='website security, gdpr certification, gdpr solutions, gdpr compliance website, test website security, website vulnerability, data breach gdpr, tester la sécurité du site web, sécurité du site, conformité GDPR, vulnérabilité du site, violation de données gdpr' />
        </Helmet>

        {content}

        <div id="bg"></div>
      </div>
    )
  }
}

export default Template

export const pageQuery = graphql`
  query PageQuery {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`