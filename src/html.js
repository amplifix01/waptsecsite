import React from 'react';

export default class HTML extends React.Component {
  render() {

   let googleAdsScript = (<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129315713-4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-129315713-4');
    </script>)
      
    return (
      
      <html lang="en">
        <head>
          
          {googleAdsScript}

          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <meta name="version" content='2.0.9' />

          {this.props.headComponents}
       </head>
        <body>
          <div
            key = {`head`}
            id="___gatsby"
            dangerouslySetInnerHTML={{ __html: `<meta name="version" content='2.0.9-1' />` }}
          />
          <div
            id="___gatsby"
            dangerouslySetInnerHTML={{ __html: this.props.body }}
          />
          {this.props.postBodyComponents}
        </body>
      </html>
    );
  }
}