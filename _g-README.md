# How to build site

## Install
`yarn`

## Run
`gatsby develop`

## IPFS Deploy
gatsby build --prefix-paths .

## Description

Gatsby starter based on the Dimension site template, designed by [HTML5 UP](https://html5up.net/dimension). Check out https://codebushi.com/gatsby-starters/ for more Gatsby starters and templates.

### Dimension Preview

http://gatsby-dimension.surge.sh/

### New Dimension Site

Install this starter (assuming Gatsby is installed) by running from your CLI:
`gatsby new gatsby-starter-dimension https://github.com/ChangoMan/gatsby-starter-dimension`

Run `gatsby develop` in the terminal to start.
